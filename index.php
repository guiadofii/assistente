<?php
if ($_SERVER['REQUEST_METHOD']!='POST') header("HTTP/1.1 404");
else{
  date_default_timezone_set('America/Bahia');
  define('BOT_TOKEN', 'A_TOKEN_AQUI');
  define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');
  define('servername', 'HOST_DO_DB');
  define('username', 'USUARIO_DO_DB');
  define('password', 'SENHA_DO_DB');
  define('database', 'NOME_DO_DB');
  
  function apiRequestJson($method, $parameters) {
      $parameters['method'] = $method;
      $handle = curl_init(API_URL);
      curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
      curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_exec($handle);
      curl_close($handle);
      return true;
  }
  function apiRequest($method, $parameters){
      $parameters['method'] = $method;
      $handle = curl_init(API_URL);
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
      curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      $exe = curl_exec($handle);
      curl_close($handle);
      return $exe;
  }
  function translate($text){
      switch ($text) {
        case 'Bad Request: not enough rights to restrict/unrestrict chat member':
          $translate = 'Desculpe eu não tenho as permissões nescessárias para tal ação';
          break;
        case 'Bad Request: user is an administrator of the chat':
          $translate = 'Desculpe ele é um adiministrador não posso fazer isso';
          break;
        default:
          $translate = $text;
          break;
      }
      return $translate;
  }
  function convert_group($ant_chat_id, $new_chat_id){
    if (is_file('rules/'.$ant_chat_id.'.apr')) {
      rename('rules/'.$ant_chat_id.'.apr', 'rules/'.$new_chat_id.'.apr');
    }
    if (is_file('welcome/'.$ant_chat_id.'.apr')) {
      rename('welcome/'.$ant_chat_id.'.apr', 'welcome/'.$new_chat_id.'.apr');
    }
    if (is_file('link/'.$ant_chat_id.'.apr')) {
      rename('link/'.$ant_chat_id.'.apr', 'link/'.$new_chat_id.'.apr');
    }
    apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Nossa, parece que este grupo foi convertido a um supergroupo, todas as advertências que não atingiram o limite de 3 foram perdidas', 'reply_to_message_id' => $message_id));
  }
  function group($id){
    $conn = new PDO('mysql:host='.servername.';dbname='.database, username, password);
    $exists = false;
    $conn2 = $conn;
    $stmt = $conn2->prepare('SELECT * FROM group WHERE id = ?;');
    $stmt->bindValue(1, $id);
    $stmt->execute();
    if (!is_bool($stmt)) {
      while ($linha = $stmt->fetch(PDO::FETCH_ASSOC)) {
        if ($linha['id'] == $id) {
          $exists = true;
        }
      }
    }
    if (!$exists===true) {
      $stmt = $conn->prepare('INSERT INTO group VALUE (?);');
      $stmt->bindValue(1, $id);
      $stmt->execute();
    }
  }
  function user($id){
      try {
          $conn = new PDO('mysql:host='.servername.';dbname='.database, username, password);
      } catch (PDOException $e) {
          return 'Conexão com o banco de dados falhou, tente novamente';
      }
      $exists = false;
      $conn2 = $conn;
      $query = $conn2->query('SELECT * FROM atheckos;');
      while ($linha = $query->fetch(PDO::FETCH_ASSOC)) {
          if ($linha['id'] == $id) {
            $exists = true;
          }
      }
      if ($exists === true) {
            return 'Olá  !first !last, seja bem vindo (a). Eu sou o atheckos, o seu assistente e tenho muitas funções que eu tenho certeza que você vai adorar.';
      }
      else{
            $stmt = $conn->prepare('INSERT INTO atheckos VALUE (?, ?);');
            $stmt->bindValue(1, $id);
            $stmt->bindValue(2, true);
            if ($stmt->execute()){           
              return 'Olá  !first !last, seja bem vindo (a). Eu sou o atheckos, o seu assistente e tenho muitas funções que eu tenho certeza que você vai adorar.';
            }
            else{
                return 'Desculpe houve um erro tente enviar /start novamente';
            }
      }
  }
  function processMessage($message) {
    $message_id = $message['message_id'];
    $chat_id = $message['chat']['id'];
    $user_id = $message['from']['id'];

    if ($message['chat']['type'] == 'group' or $message['chat']['type'] == 'supergroup') {
      group($chat_id);
    }

    if ($message['chat']['type'] == 'private' and empty($message['processInline'])) {
      apiRequestJson('sendChatAction', array('chat_id' => $chat_id, 'action' => 'typing'));
    }
    
    if (isset($message['text'])) {
      $text = $message['text'];

      if ($message['chat']['type'] == 'private') {
        if (stripos($text, '/start') === 0) {
            $text = str_ireplace('/start ', '', $text);
            $text = base64_decode($text);
            if (stripos($text, 'rules/')===0) {
              if (file_exists($text.'.apr')) {
                $return = file_get_contents($text.'.apr');
                $return = str_ireplace('!first', $message['from']['first_name'], $return);
                $return = str_ireplace('!last', $message['from']['last_name'], $return);
                $return = str_ireplace('!username', $message['from']['username'], $return);
                apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $return, 'parse_mode' => 'HTML', 'reply_to_message_id' => $message_id));
                user($chat_id);
                return true;
              }
              else{
                    $id = str_ireplace('rules/', '', $text);
                    $exe = apiRequest('getChat', array('chat_id' => $id));
                    $result = json_decode($exe, true);
                    if ($result['ok'] == true) {
                        $title = isset($result['result']['title']) ? $result['result']['title'] : 'Nome desconhecido';
                        $username = isset($result['result']['username']) ? $result['result']['username'] : '-';
                        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Não encotramos as regras para o grupo ' . $title, 'reply_to_message_id' => $message_id));
                        return true;
                    } else {
                        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Este grupo não existe', 'reply_to_message_id' => $message_id));
                        return true;
                    }
                }
            }
            else{
              $return = user($chat_id);
              $return = str_replace('!first', $message['from']['first_name'], $return);
              $return = str_replace('!last', $message['from']['last_name'], $return);
              apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $return, 'reply_to_message_id' => $message_id));
              return true;
            }
        }
      }
      elseif (stripos($text, '/start')===0 and ($message['chat']['type'] == 'group' or $message['chat']['type'] == 'supergroup')) {
           apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Olá eu sou um robô que foi feito para tornar seu grupo ou canal melhor e mais intuitivo conheça mais sobre o que eu posso fazer no meu canal', 'reply_to_message_id' => $message_id, 'reply_markup' => array('inline_keyboard' => array(array(array('text' => 'Vê meu canal', 'url' => 'https://t.me/AtheckosRobot'))))));
           return true;
      }elseif ($text == '/removerbot' or stripos($text, 'atheckos saia do grupo')===0 and ($message['chat']['type'] == 'group' or $message['chat']['type'] == 'supergroup'))
{
      $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
              $user = json_decode($exe, true);
              if ($user['result']['status'] === 'creator') {
                         apiRequestJson('leaveChat', array('chat_id' => $chat_id));
                  
                  return true;
              }
              else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é o criador!', 'reply_to_message_id' => $message_id));
                  return true;
              }
          }
      elseif(isset($message['reply_to_message'])) {
          if (stripos($text, '/advert')===0 or stripos($text, 'atheckos dê um advert nele')===0) {
            $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
            $user = json_decode($exe, true);
            if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
              $ad = $message['reply_to_message']['from']['id'];
              if (file_exists('adverts/'.$ad.' in '.$chat_id.'.apr')){
                $num = file_get_contents('adverts/'.$ad.' in '.$chat_id.'.apr');
                $num = intval($num);
                $num++;
                file_put_contents('adverts/'.$ad.' in '.$chat_id.'.apr', $num);
                if ($num==3) {
                  $exe = apiRequest('kickChatMember', array('chat_id' => $chat_id, 'user_id' => $ad, 'until_date' => time()));
                  $result = json_decode($exe, true);
                  if ($result['ok']==true) {
                    apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O usuário foi banido por atingir o número máximo de advertências', 'reply_to_message_id' => $message_id));
                    return true;
                  }
                  else{
                    apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => translate($result['description']), 'reply_to_message_id' => $message_id));
                    return true;
                  }
                }
                else{
                  apiRequestJson('sendMessage', array('chat_id'=>$chat_id, 'text'=> 'O usuário foi advertido'."\n \n".'Total de advertências: 2 de 3','reply_to_message_id' => $message_id));
                  return true;
                }
              }
              else{
                $handle = fopen('adverts/'.$ad.' in '.$chat_id.'.apr', 'a');
                fwrite($handle, 1);
                fclose($handle);
                apiRequestJson('sendMessage', array('chat_id'=>$chat_id, 'text'=> 'O usuário foi advertido'."\n \n".'Total de advertências: 1 de 3','reply_to_message_id' => $message_id));
                return true;
              }
            }
            else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
                  return true;
            }
          }
          elseif (stripos($text, '/ban')===0 or stripos($text, 'atheckos dê um ban nele')===0) {
              $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
              $user = json_decode($exe, true);
              if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {

                  $ban = $message['reply_to_message']['from']['id'];
                  $exe = apiRequest('kickChatMember', array('chat_id' => $chat_id, 'user_id' => $ban, 'until_date' => time()));
                  $result = json_decode($exe, true);
                  if ($result['ok']==true) {
                  	apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O usuário foi banido!', 'reply_to_message_id' => $message_id));
                    return true;
                  }
                  else{
                  	apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => translate($result['description']), 'reply_to_message_id' => $message_id));
                      return true;
                  }
              }
              else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
                  return true;
              }
          }
          elseif (stripos($text, '/kick')===0 or stripos($text, 'atheckos remova ele')===0) {
              $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
              $user = json_decode($exe, true);
              if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {

                  $ban = $message['reply_to_message']['from']['id'];
                  $exe = apiRequest('kickChatMember', array('chat_id' => $chat_id, 'user_id' => $ban));
                  $result = json_decode($exe, true);
                  if ($result['ok']==true) {
                    apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O usuário foi removido!', 'reply_to_message_id' => $message_id));
                    return true;
                  }
                  else{
                    apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => translate($result['description']), 'reply_to_message_id' => $message_id));
                      return true;
                  }
              }
              else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
                  return true;
              }
          }
          elseif (stripos($text, '/unban')===0 or stripos($text, 'atheckos libere ele')===0) {
              $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
              $user = json_decode($exe, true);
              if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
                  $ban = $message['reply_to_message']['from']['id'];
                  $exe = apiRequest('unbanChatMember', array('chat_id' => $chat_id, 'user_id' => $ban));
                  $result = json_decode($exe, true);
                  if ($result['ok']==true) {
                  	apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Privelégios normais de membros restaurados!', 'reply_to_message_id' => $message_id));
                    return true;
                  }
                  else{
                  	apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => translate($result['description']), 'reply_to_message_id' => $message_id));
                    return true;
                  }
              }
              else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
                  return true;
              }
          }
          
          elseif (stripos($text, '/pin')===0 or stripos($text, 'atheckos fixe isso')===0) {
              $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
              $user = json_decode($exe, true);
              if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
                  $message_id = $message['reply_to_message']['message_id'];
                  apiRequestJson('pinChatMessage', array('chat_id' => $chat_id, 'message_id' => $message_id));
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Mensagem Fixada', 'reply_to_message_id' => $message_id));
                  return true;
              }
              else {
                  apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
                  return true;
              }
          }
          elseif (stripos($text, 'Qual o id dele atheckos')===0 or stripos($text, 'atheckos qual o id dele')===0) {
            $id = $message['reply_to_message']['from']['id'];
            apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O id dele é '.$id, 'reply_to_message_id' => $message_id));
            return true;
        }
      }
      elseif (stripos($text, '/unpin')===0 or stripos($text, 'atheckos desfixe')===0){
          $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
          $user = json_decode($exe, true);
          if ($user['result']['status']==='administrator' or $user['result']['status']==='creator'){
            apiRequestJson('unpinChatMessage', array('chat_id' => $chat_id));
            apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Mensagem Desfixada', 'reply_to_message_id' => $message_id));
            return true;
          }
          else{
            $message_id = $message['reply_to_message']['message_id'];
            apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Você não é um adiministrador!', 'reply_to_message_id' => $message_id));
            return true;
          }
      }
      elseif (stripos($text, '/regras')===0 or stripos($text, 'atheckos as regras')===0){
          $start = base64_encode('rules/'.$chat_id);
          $return = '<b>Clique no botão abaixo para vê as nossas regras</b>';
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $return, 'reply_to_message_id' => $message_id, 'parse_mode' => 'HTML', 'reply_markup' => array('inline_keyboard' => array(array(array('text' => 'Lê as regras', 'url' => 'https://t.me/AtheckosBot?start='.$start))))));
          return true;
      }
      elseif (stripos($text, '/setregras')===0 or stripos($text, 'atheckos as regras do grupo serão')===0){
        $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
        $user = json_decode($exe, true);
        if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
           if (stripos($text, '/setregras')===0) {
            $text = str_ireplace('/setregras ', '', $text);
          }
          elseif (stripos($text, 'atheckos as regras do grupo serão')===0){
            $text = str_ireplace('atheckos as regras do grupo serão ', '', $text);
          }
          $text = str_ireplace('/setregras ', '', $text);
          fopen('rules/'.$chat_id.'.apr', 'a');
          file_put_contents('rules/'.$chat_id.'.apr', $text);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Regras salvas', 'reply_to_message_id' => $message_id));
          return true;
        }
        else{
          apiRequestJson('sendMessage', array('chat_id'=>$chat_id, 'text' => 'Você não é um administrator!', 'reply_to_message_id'=>$message_id));
          return true;
        }
      }
      elseif (stripos($text, '/link')===0){
          if (file_exists('link/'.$chat_id.'.apr')){
            $text = file_get_contents('link/'.$chat_id.'.apr', $text);
            apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $text, 'reply_to_message_id' => $message_id));
            return true;
          }
          else{
            apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Nada foi definido ainda', 'reply_to_message_id' => $message_id));
            return true;
          }
      }
      elseif (stripos($text, '/setlink')===0 or stripos($text, 'atheckos o link do grupe é')===0){
        $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
        $user = json_decode($exe, true);
        if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
          if (stripos($text, '/setlink')===0) {
            $text = str_ireplace('/setlink ', '', $text);
          }
          elseif (stripos($text, 'atheckos o link do grupe é')===0){
            $text = str_ireplace('atheckos o link do grupe é ', '', $text);
          }
          fopen('link/'.$chat_id.'.apr', 'a');
          file_put_contents('link/'.$chat_id.'.apr', $text);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Link salvo', 'reply_to_message_id' => $message_id));
          return true;
        }
        else{
          apiRequestJson('sendMessage', array('chat_id'=>$chat_id, 'text' => 'Você não é um administrator!', 'reply_to_message_id'=>$message_id));
            return true;
        }
      }
      elseif (stripos($text, '/admins')===0 or stripos($text, 'atheckos quem são os adiministradores')===0 or stripos($text, 'Quem são os adiministradores atheckos')===0) {
        $exe = apiRequest("getChatAdministrators", array('chat_id' => $chat_id));
        $exe = json_decode($exe, true);
        if ($exe['ok']===true) {
          $admins = $exe['result'];
          $i=0;
          $response = '*Os adiministradores deste grupo estão listados abaixo :*'."\n \n";
          while (isset($admins[$i]['user'])) {
            if ($admins[$i]['status']==='creator') {
              $response .= '🤴 ['.$admins[$i]['user']['first_name'].'](tg://user?id='.$admins[$i]['user']['id'].')'."\n \n";
            }
            elseif ($admins[$i]['user']['is_bot']) {
              $response .= '🕹 ['.$admins[$i]['user']['first_name'].'](tg://user?id='.$admins[$i]['user']['id'].')'."\n \n";
            }
            else{
              $response .= '👮 ['.$admins[$i]['user']['first_name'].'](tg://user?id='.$admins[$i]['user']['id'].')'."\n \n";
            }
            $i++;
          }
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $response, 'reply_to_message_id' => $message_id, 'parse_mode' => 'Markdown'));
        }
        else{
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Desculpe, houve algum erro ao obter a lista de adiministradores', 'reply_to_message_id' => $message_id));
        }
      }
      
      elseif (stripos($text, '/bemvindo')===0 or stripos($text, 'atheckos a mensagem de boas vindas será')===0){
        $exe = apiRequest('getChatMember', array('chat_id' => $chat_id, 'user_id' => $user_id));
        $user = json_decode($exe, true);
        if ($user['result']['status'] === 'administrator' or $user['result']['status'] === 'creator') {
          if (stripos($text, '/bemvindo')===0) {
            $text = str_ireplace('/bemvindo ', '', $text);
          }
          elseif (stripos($text, 'atheckos a mensagem de boas vindas será')===0){
            $text = str_ireplace('atheckos a mensagem de boas vindas será ', '', $text);
          }
          fopen('welcome/'.$chat_id.'.apr', 'a');
          file_put_contents('welcome/'.$chat_id.'.apr', $text);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Mensagem de boas vindas salva', 'reply_to_message_id' => $message_id));
          return true;
        }
        else{
          apiRequestJson('sendMessage', array('chat_id'=>$chat_id, 'text' => 'Você não é um administrator!', 'reply_to_message_id'=>$message_id));
            return true;
        }
      }
      elseif (stripos($text, 'Qual o meu id atheckos')===0 or stripos($text, 'atheckos qual o meu id')===0) {
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O id seu id é '.$user_id, 'reply_to_message_id' => $message_id));
        return true;
      }
      elseif (stripos($text, 'Qual o id do chat atheckos')===0 or stripos($text, 'atheckos qual o id do chat')===0) {
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'O id do chat é '.$chat_id, 'reply_to_message_id' => $message_id));
        return true;
      }
      $command=false;
      $bot = explode('@', $text);
      $i=0;
      while (isset($message['entities'][$i])) {
        if ($message['entities'][$i]['type']=='bot_command' and stripos($bot[1], 'atheckosBot')===0){$command=true;break;}
        $i++;
      }
      if ($command===true) {
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Desculpe, este comando não é suportado por mim ou possui sintaxe inválida verifique meu canal de updates para conheçer mais sobre meus comandos', 'reply_to_message_id' => $message_id, 'reply_markup' => array('inline_keyboard' => array(array(array('text' => 'Vê meu canal', 'url' => 'https://t.me/AtheckosRobot'))))));
        return true;
      }
      elseif ($message['chat']['type'] == 'private'){
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Desculpe, não estou a compreender isso 👆', 'reply_to_message_id' => $message_id));
        return true;
      }
    }
    elseif (isset($message['new_chat_members'])) {
     if(strlen($message['new_chat_member']['first_name'])>=35 or strlen($message['new_chat_member']['last_name'])>=35)
        {$ban = $message['new_chat_member']['id'];
                  $exe = apiRequest('kickChatMember', array('chat_id' => $chat_id, 'user_id' => $ban));
             $result = json_decode($exe, true);
                  if($result['ok']==true){
          group($chat_id);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Removido por ter o nome muito grande. <b>Isso pode ser considerado flood</b>', 'reply_to_message_id' => $message_id, 'parse_mode' => 'html'));
         return true; 
        }
        }   
        elseif (file_exists('welcome/'.$chat_id.'.apr')) {
          $name['user'] = isset($message['new_chat_member']['username'])?'@'.$message['new_chat_member']['username']:'-';
          $return = file_get_contents('welcome/'.$chat_id.'.apr');
          $return = str_ireplace('!first', $message['new_chat_member']['first_name'], $return);
          $return = str_ireplace('!last', $message['new_chat_member']['last_name'], $return);
          $return = str_ireplace('!username', $name['user'], $return);
          $start = base64_encode('rules/'.$chat_id);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => $return, 'parse_mode' => 'HTML', 'reply_to_message_id' => $message_id,'disable_web_page_preview' => true, 'reply_markup' => array('inline_keyboard' => array(array(array('text' => 'Lê as regras', 'url' => 'https://t.me/AtheckosBot?start='.$start))))));
          return true;
        }
        elseif (stripos($message['new_chat_member']['username'], 'atheckosBot')===0 and strlen($message['new_chat_member']['username'])===11) {
          group($chat_id);
          apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' => 'Olá eu sou um robô que foi feito para tornar seu grupo ou canal melhor e mais intuitivo conheça mais sobre o que eu posso fazer no meu canal', 'reply_to_message_id' => $message_id, 'reply_markup' => array('inline_keyboard' => array(array(array('text' => 'Vê meu canal', 'url' => 'https://t.me/AtheckosRobot'))))));
        }  
       
        return true;
    }
    elseif (isset($message['left_chat_member'])) {;
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' =>'Adeus '.$message['left_chat_member']['first_name']. '👋 ...', 'reply_to_message_id' => $message_id));
        return true;
    }
    elseif (isset($message['migrate_from_chat_id'])){
      convert_group($message['migrate_from_chat_id'], $chat_id);
    }
    elseif (isset($message['migrate_to_chat_id'])) {
      convert_group($chat_id, $message['migrate_to_chat_id']);
    }
    elseif ($message['chat']['type'] == 'private'){
        apiRequestJson('sendMessage', array('chat_id' => $chat_id, 'text' =>'Mande apenas mensagens de texto.', 'reply_to_message_id' => $message_id));
        return true;
    }
    return true;
  }

  function processInline($query){
      $q_id = $query['id'];
      $q_text = $query['query'];
      if ($q_text==='assistente'){
        $array = array('type' => 'photo', 'id' => base64_encode(sha1($q_id)), 'title' => 'assistente', 'description' => 'assistente no inline', 'photo_width' => 1280, 'photo_height' => 720, 'thumb_url' => 'https://atheckos.github.io/api/assistente/ads/img/assistente-ads.jpg', 'photo_url' => 'https://atheckos.github.io/api/assistente/ads/img/assistente-ads.jpg', 'caption' => 'Um robô administrator de grupos e que pode divulgar canais.', 'reply_markup' => array('inline_keyboard' => array(array(array('text' => '🏠 Canal', 'url' => 'https://t.me/Atheckos')), array(array('text' => '👥 Grupo', 'url' => 'https://t.me/grupoatheckos')), array(array('text' => 'Compartilhar', 'switch_inline_query' => 'assistente')))));
      }
      apiRequestJson('answerInlineQuery', array('inline_query_id' => $q_id,  'results' => array($array)));
  }

  $content = file_get_contents('php://input');
  $update = json_decode($content, true);
  if (isset($update['message'])) {
    processMessage($update['message']);
  }
  elseif (isset($update['inline_query'])) {
    processInline($update['inline_query']);
  }
}